export interface IDataChart {
  id: number;
  name: string;
  日: number;
  週: number;
  月: number;
  年: number;
}

export interface ILabelChart {
  id: number;
  name: string;
  stroke: string;
  strokeWidth: number
}
import { ICategoryItem } from "@/types/category";

export interface IFoodItem {
  id: number;
  image: string;
  name: string;
  descriptions: string;
  date: string;
  category: ICategoryItem;
}
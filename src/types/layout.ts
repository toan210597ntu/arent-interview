export interface IMenuItem {
  id: number;
  href: string;
  name: string;
  icon: string;
  isBadge?: boolean;
}

export interface IFooterItem {
  id: number;
  name: string;
  link: string;
}

export interface IDropdownItem {
  id: number;
  name: string;
  link: string;
}
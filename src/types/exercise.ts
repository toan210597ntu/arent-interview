export interface IExerciseItem {
  id: number;
  title: string;
  time: number;
  kcal: number;
}
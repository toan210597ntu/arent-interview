export interface IRecordItem {
  id: number;
  name: string;
  image: string;
  CTA: string;
}
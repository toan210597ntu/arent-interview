export interface IRecommendedItem {
  id: number;
  name: string;
  descriptions: string;
}

export interface ILivingItem {
  id: number;
  name: string;
  date: string;
  time: string;
  image: string;
  descriptions: string;
  CTA: string;
}
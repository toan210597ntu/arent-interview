export interface IDiaryItem {
  id: number;
  date: string;
  time: string;
  title: string;
  descriptions: string;
}
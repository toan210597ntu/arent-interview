export enum Category {
  MORNING = 'Morning',
  LUNCH = 'Lunch',
  DINNER = 'Dinner',
  SNACK = 'Snack'
}

export interface ICategoryItem {
  id: number;
  name: Category;
  icon: string;
}
import { Category, ICategoryItem } from "@/types/category";

export const categories: ICategoryItem[] = [
  {
    id: 1,
    name: Category.MORNING,
    icon: '/images/icons/icon_knife.svg'
  },
  {
    id: 2,
    name: Category.LUNCH,
    icon: '/images/icons/icon_knife.svg'
  },
  {
    id: 3,
    name: Category.DINNER,
    icon: '/images/icons/icon_knife.svg'
  },
  {
    id: 4,
    name: Category.SNACK,
    icon: '/images/icons/icon_cup.svg'
  },
]
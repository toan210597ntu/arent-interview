import { IFoodItem } from "@/types/food";
import { categories } from "./categories";

export const foods: IFoodItem[] = [
  {
    id: 1,
    image: '/images/foods/m01.jpg',
    name: 'Cake',
    date: '05.21',
    category: categories[0],
    descriptions: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Vero, repellat.',
  },
  {
    id: 2,
    image: '/images/foods/l01.jpg',
    name: 'Sushi',
    date: '05.21',
    category: categories[1],
    descriptions: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Vero, repellat.',
  },
  {
    id: 3,
    image: '/images/foods/d01.jpg',
    name: 'Rice',
    date: '05.21',
    category: categories[2],
    descriptions: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Vero, repellat.',
  },
  {
    id: 4,
    image: '/images/foods/m01.jpg',
    name: 'Cake',
    date: '05.21',
    category: categories[3],
    descriptions: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Vero, repellat.',
  },
  {
    id: 5,
    image: '/images/foods/m01.jpg',
    name: 'Cake',
    date: '05.20',
    category: categories[0],
    descriptions: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Vero, repellat.',
  },
  {
    id: 6,
    image: '/images/foods/l02.jpg',
    name: 'Hot Dog',
    date: '05.20',
    category: categories[1],
    descriptions: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Vero, repellat.',
  },
  {
    id: 7,
    image: '/images/foods/d02.jpg',
    name: 'Cake',
    date: '05.20',
    category: categories[2],
    descriptions: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Vero, repellat.',
  },
  {
    id: 8,
    image: '/images/foods/s01.jpg',
    name: 'Cake',
    date: '05.20',
    category: categories[3],
    descriptions: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Vero, repellat.',
  },
]
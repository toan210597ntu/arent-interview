import { IDropdownItem, IFooterItem, IMenuItem } from "@/types/layout";

export const menus: IMenuItem[] = [
  {
    id: 1,
    href: '/my-record',
    icon: '/images/icons/icon_memo.svg',
    name: '自分の記録'
  },
  {
    id: 2,
    href: '/my-column',
    icon: '/images/icons/icon_challenge.svg',
    name: 'チャレンジ',
  },
  {
    id: 3,
    href: '/my-notification',
    icon: '/images/icons/icon_info.svg',
    name: 'お知らせ',
    isBadge: true,
  },
]

export const footerLinks: IFooterItem[] = [
  {
    id: 1,
    name: '会員登録',
    link: '/'
  },
  {
    id: 2,
    name: '運営会社',
    link: '/'
  },
  {
    id: 3,
    name: '利用規約',
    link: '/'
  },
  {
    id: 4,
    name: '個人情報の取扱について',
    link: '/'
  },
  {
    id: 5,
    name: '特定商取引法に基づく表記',
    link: '/'
  },
  {
    id: 6,
    name: 'お問い合わせ',
    link: '/'
  },
]

export const dropdowns: IDropdownItem[] = [
  {
    id: 1,
    name: '自分の記録',
    link: '/'
  },
  {
    id: 2,
    name: '体重グラフ',
    link: '/'
  },
  {
    id: 3,
    name: '目標',
    link: '/'
  },
  {
    id: 4,
    name: '選択中のコース',
    link: '/'
  },
  {
    id: 5,
    name: 'コラム一覧',
    link: '/'
  },
  {
    id: 6,
    name: '設定',
    link: '/'
  },
]
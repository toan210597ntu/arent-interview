import { ILivingItem, IRecommendedItem } from "@/types/recommended";

export const recommends: IRecommendedItem[] = [
  {
    id: 1,
    name: 'RECOMMENDED COLUMN',
    descriptions: 'オススメ'
  },
  {
    id: 2,
    name: 'RECOMMENDED DIET',
    descriptions: 'ダイエット'
  },
  {
    id: 3,
    name: 'RECOMMENDED BEAUTY',
    descriptions: '美容'
  },
  {
    id: 4,
    name: 'RECOMMENDED HEALTH',
    descriptions: '健康'
  },
]

export const livings: ILivingItem[] = [
  {
    id: 1,
    date: '2021.05.17',
    time: '23:25',
    image: '/images/livings/column-1.jpg',
    name: '魚を食べて頭もカラダも元気に',
    descriptions: '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…',
    CTA: '#魚料理  #和食  #DHA'
  },
  {
    id: 2,
    date: '2021.05.17',
    time: '23:25',
    image: '/images/livings/column-2.jpg',
    name: '魚を食べて頭もカラダも元気に',
    descriptions: '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…',
    CTA: '#魚料理  #和食  #DHA'
  },
  {
    id: 3,
    date: '2021.05.17',
    time: '23:25',
    image: '/images/livings/column-3.jpg',
    name: '魚を食べて頭もカラダも元気に',
    descriptions: '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…',
    CTA: '#魚料理  #和食  #DHA'
  },
  {
    id: 4,
    date: '2021.05.17',
    time: '23:25',
    image: '/images/livings/column-4.jpg',
    name: '魚を食べて頭もカラダも元気に',
    descriptions: '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…',
    CTA: '#魚料理  #和食  #DHA'
  },
  {
    id: 5,
    date: '2021.05.17',
    time: '23:25',
    image: '/images/livings/column-5.jpg',
    name: '魚を食べて頭もカラダも元気に',
    descriptions: '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…',
    CTA: '#魚料理  #和食  #DHA'
  },
  {
    id: 6,
    date: '2021.05.17',
    time: '23:25',
    image: '/images/livings/column-6.jpg',
    name: '魚を食べて頭もカラダも元気に',
    descriptions: '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…',
    CTA: '#魚料理  #和食  #DHA'
  },
  {
    id: 7,
    date: '2021.05.17',
    time: '23:25',
    image: '/images/livings/column-7.jpg',
    name: '魚を食べて頭もカラダも元気に',
    descriptions: '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…',
    CTA: '#魚料理  #和食  #DHA'
  },
  {
    id: 8,
    date: '2021.05.17',
    time: '23:25',
    image: '/images/livings/column-8.jpg',
    name: '魚を食べて頭もカラダも元気に',
    descriptions: '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…',
    CTA: '#魚料理  #和食  #DHA'
  },
]
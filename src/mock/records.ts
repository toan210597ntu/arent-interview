import { IRecordItem } from "@/types/record";

export const records: IRecordItem[] = [
  {
    id: 1,
    name: 'BODY RECORD',
    image: '/images/records/record-1.jpg',
    CTA: '自分のカラダの記録'
  },
  {
    id: 2,
    name: 'MY EXERCISE',
    image: '/images/records/record-2.jpg',
    CTA: '自分の運動の記録'
  },
  {
    id: 3,
    name: 'MY DIARY',
    image: '/images/records/record-3.jpg',
    CTA: '自分の日記'
  },
]
import Image from "next/image"
import Button from "@/components/Button"
import Categories from "@/components/Categories"
import FoodCard from "@/components/FoodCard"
import { foods } from "@/mock/foods"
import { IFoodItem } from "@/types/food"


const ModFood = () => {
  const renderFoods = foods?.map((food: IFoodItem) => {
    return <FoodCard key={food.id} item={food}/>
  })
  return (
    <section className="mod-food container mx-auto mt-6.5 mb-16">
      <Categories/>
      <div className="mt-6.5 grid grid-cols-4 gap-2">
        { renderFoods }
      </div>
      <Button as="button" className="mt-7 mx-auto">記録をもっと見る</Button>
    </section>
  )
}

export default ModFood
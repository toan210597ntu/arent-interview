import LivingCard from "@/components/LivingCard"
import RecommendedCard from "@/components/RecommendedCard"
import { livings, recommends } from "@/mock/recommends"
import Button from "@/components/Button"


const ModRecommended = () => {
  const renderRecommends = recommends?.map(recommend => {
    return <RecommendedCard key={recommend.id} item={recommend}/>
  })

  const renderLivings = livings?.map(living => {
    return <LivingCard key={living.id} item={living}/>
  })

  return (
    <section className="mod-recommended container mx-auto mt-14 mb-16">
      <div className="grid grid-cols-4 gap-x-8">
        {renderRecommends}
      </div>
      <div className="mt-14 grid grid-cols-4 gap-2">
        {renderLivings}
      </div>
      <Button as="button" className="mt-7.5 mx-auto">コラムをもっと見る</Button>
    </section>
  )
}

export default ModRecommended
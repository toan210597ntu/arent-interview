import Image from 'next/image'
import LineChartCustom from '@/components/LineChart'
import { bannerData, labels } from '@/mock/charts'

const ModBanner = () => {
  return (
    <section className="mod-banner">
      <div className="flex ">
        <div className='w-2/5 relative h-78 flex-shrink-0'>
          <Image
            alt="banner home"
            src="/images/banner.jpg"
            className='object-cover'
            fill
          />
        </div>
        <div className='w-3/5 bg-dark flex-1 pl-15 pr-25 py-2'>
          <LineChartCustom labels={labels} data={bannerData}/>
        </div>
      </div>
    </section>
  )
}

export default ModBanner
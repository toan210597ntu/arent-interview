import RecordCard from "@/components/RecordCard"
import { records } from "@/mock/records"


const ModThreeCol = () => {
  const renderRecord = records?.map(record => {
    return (<RecordCard key={record.id} item={record}/>)
  })
  return (
    <section className="mod-three-col container mx-auto mt-14">
      <div className="flex gap-x-12 justify-center">
        {renderRecord}
      </div>
    </section>
  )
}

export default ModThreeCol
import Link from "next/link"
import { footerLinks } from "@/mock/layout"

const ModFooter = () => {
  const renderLink = footerLinks?.map(link => {
    return (
      <Link key={link.id} className="text-light mr-12 text-11 font-Noto" href={link.link}>
        {link.name}
      </Link>
    )
  })
  return (
    <footer className="bg-dark-500 py-14">
      <nav className="container">
        {renderLink}
      </nav>
    </footer>
  )
}

export default ModFooter
'use client';

import { useState } from "react"
import Link from "next/link"
import Image from "next/image"
import { IMenuItem } from "@/types/layout"
import { menus } from "@/mock/layout"

// components
import NavItem from "@/components/NavItem"
import Dropdown from "@/components/Dropdown"


const ModHeader = () => {
  const [showMenu, setShowMenu] = useState<boolean>(false)
  const renderMenus = menus?.map((menu: IMenuItem) => {
    return <NavItem key={menu.id} item={menu} />
  })

  const handleShowMenu = () => {
    setShowMenu(!showMenu)
  }

  return (
    <header className="bg-dark-500 sticky top-0 w-full right-0 left-0 z-50">
      <div className="container mx-auto flex items-center justify-between">
        <Link href={'/'} className="pt-4 pb-2">
          <Image
            alt="Healthy"
            src="/images/logo.png"
            width={109} height={40}
          />
        </Link>
        <nav className="flex items-center text-light">
          { renderMenus }
          <div className="relative z-60">
            <button
              type="button"
              onClick={handleShowMenu}
            >
              <Image
                alt="hamburger"
                src={showMenu ? '/images/icons/icon_close.svg' : '/images/icons/icon_menu.svg'}
                width={32} height={32}
              />
            </button>
            <Dropdown className={`${ showMenu ? 'active' : '' } dropdown absolute w-70 right-0`}/>
          </div>
        </nav>
      </div>
    </header>
  )
}

export default ModHeader;
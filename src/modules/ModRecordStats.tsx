import BoxRecord from "@/components/BoxRecord"
import ExerciseCard from "@/components/ExerciseCard"
import LineChartCustom from "@/components/LineChart"
import { labels, recordData } from "@/mock/charts"
import { exercises } from "@/mock/exercise"

const ModRecordStats = () => {
  const renderExercises = exercises?.map(exercise => {
    return (<ExerciseCard key={exercise.id} item={exercise}/>)
  })
  return (
    <section className="mod-record-stats container mx-auto mt-14">
      <BoxRecord
        title="BODY<br/>RECORD"
        date="2021.05.21"
      >
        <div className="mt-2 h-58.5 px-4 w-full">
          <LineChartCustom showLegend={true} labels={labels} data={recordData} />
        </div>
      </BoxRecord>
      <BoxRecord
        title="MY<br/>EXERCISE"
        date="2021.05.21"
        className="mt-14"
      >
        <div className="grid mt-2 grid-cols-2 gap-x-10 h-48 overflow-y-scroll custom-scroll pr-6">
          {renderExercises}
        </div>
      </BoxRecord>
    </section>
  )
}

export default ModRecordStats
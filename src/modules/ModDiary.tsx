import { diaries } from "@/mock/diaries"

// components
import DiaryCard from "@/components/DiaryCard"
import Button from "@/components/Button"

const ModDiary = () => {
  const renderDiaries = diaries?.map(diary => {
    return <DiaryCard key={diary.id} item={diary}/>
  })

  return (
    <section className="mod-diary container mx-auto mt-14 mb-16">
      <h2 className="uppercase text-22">My Diary</h2>
      <div className="grid grid-cols-4 gap-3 mt-1">
        {renderDiaries}
      </div>
      <Button as="button" className="mt-8.5 mx-auto">自分の日記をもっと見る</Button>
    </section>
  )
}

export default ModDiary
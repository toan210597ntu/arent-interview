import ModDiary from '@/modules/ModDiary'
import ModRecordStats from '@/modules/ModRecordStats'
import ModThreeCol from '@/modules/ModThreeCol'

export default function MyRecord() {
  return (
    <>
      <ModThreeCol />
      <ModRecordStats/>
      <ModDiary />
    </>
  )
}

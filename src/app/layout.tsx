import Image from "next/image"
import "@/styles/global.scss"
import Header from "@/modules/ModHeader"
import Footer from "@/modules/ModFooter"
import Button from "@/components/Button"
export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <head />
      <body>
        <Header />
        <main className="relative">
          {children}
          <button
            type="button"
            className="fixed top-1/2 translate-y-50 right-16 w-12 h-12 flex items-center justify-center rounded-full border border-gray-400"
          >
            <Image
              alt="icon-arrow"
              src="/images/icons/icon_arrow.svg"
              width={15}
              height={8}
            />
          </button>
        </main>
        <Footer/>
      </body>
    </html>
  )
}

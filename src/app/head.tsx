export default function Head() {
  return (
    <>
      <title>Health App</title>
      <meta content="width=device-width, initial-scale=1" name="viewport" />
      <meta name="description" content="A web application for health care." />
      <link rel="icon" href="/favicon.ico" />
    </>
  )
}

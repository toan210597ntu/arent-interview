import ModBanner from '@/modules/ModBanner'
import ModFood from '@/modules/ModFood'

export default function Home() {
  return (
    <>
      <ModBanner />
      <ModFood />
    </>
  )
}

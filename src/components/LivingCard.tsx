import { FC } from "react"
import Link from "next/link"
import { ILivingItem } from "@/types/recommended"

interface ILivingCardProps {
  item: ILivingItem
}

const LivingCard: FC<ILivingCardProps> = ({ item }) => {
  return (
    <div className="cursor-pointer">
      <div
        className="w-58.5 h-36 bg-cover bg-no-repeat bg-center flex items-end cursor-pointer"
        style={{backgroundImage: `url('${item.image}')`}}
      >
        <span className="text-15 text-light font-Inter p-2 bg-primary-300">
          { item.date } {item.time}
        </span>
      </div>
      <div className="mt-2">
        <p className="text-description text-dark-500 line-clamp-2 font-Noto">{item.descriptions}</p>
        <Link href={'/'} className="text-cta text-primary-400 font-Noto">{item.CTA}</Link>
      </div>
    </div>
  )
}

export default LivingCard
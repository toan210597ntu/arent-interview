'use client';

import { FC } from "react"
import Link from "next/link"
import Image from "next/image"
import { usePathname } from "next/navigation"
import { IMenuItem } from "@/types/layout"

//components
import Badge from "@/components/Badge"

interface INavItemProps {
  item: IMenuItem
}

const NavItem: FC<INavItemProps> = ({ item } ) => {
  const pathName = usePathname()
  return (
    <Link
      href={item.href}
      className={`${pathName === item.href ? 'text-primary-400' : ''} font-Noto flex items-center cursor-pointer text-menu p-1.1 mr-11 hover:text-primary-400`}
    >
      { item.isBadge ?
        <Badge>
          <Image
            alt="memo category"
            src={item.icon}
            width={32} height={32}
            className="mr-2.5"
          />
        </Badge> :
          <Image
          alt="memo category"
          src={item.icon}
          width={32} height={32}
          className="mr-2.5"
        />
      }
      { item.name }
    </Link>
  )
}

export default NavItem
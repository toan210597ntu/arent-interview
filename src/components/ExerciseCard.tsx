import { FC } from "react"
import { IExerciseItem } from "@/types/exercise"

interface IExerciseCardProps {
  item: IExerciseItem
}

const ExerciseCard: FC<IExerciseCardProps> = ({ item }) => {
  return (
    <div className="flex justify-between pb-1 border-b border-gray-400">
      <div className="relative pl-4">
        <h4 className="text-description text-light font-Noto">{item.title}</h4>
        <span className="text-primary-300 text-15 font-normal">{item.kcal}kcal</span>
        <div className="w-1 h-1 rounded-full bg-light absolute left-0 top-2"></div>
      </div>
      <span className="text-primary-300 text-title">{item.time}min</span>
    </div>
  )
}

export default ExerciseCard
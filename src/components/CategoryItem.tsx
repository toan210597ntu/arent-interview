import { FC } from "react"
import Image from "next/image"
import { ICategoryItem } from "@/types/category"

interface ICategoryItemProps {
  item: ICategoryItem
}

const CategoryItem: FC<ICategoryItemProps> = ({ item }) => {
  return (
    <div
      className="relative w-29 h-33.5 flex flex-col items-center justify-center bg-cover bg-no-repeat"
      style={{ backgroundImage: `url('/images/hexagon.png')`}}
    >
      <Image
        alt={item.name}
        src={item.icon}
        width={56}
        height={56}
      />
      <h4 className="text-20 text-light">{item.name}</h4>
    </div>
  )
}

export default CategoryItem
import { AnchorHTMLAttributes, ButtonHTMLAttributes, FC, ReactNode } from 'react';
import Link, { LinkProps } from 'next/link';
/* eslint-disable-next-line */
type BaseProps = {
  children: ReactNode
  className?: string
}

type ButtonAsAction = BaseProps & Omit<ButtonHTMLAttributes<HTMLButtonElement>, keyof BaseProps> & {
  as?: 'button'
}

type ButtonAsLink = BaseProps & Omit<LinkProps, keyof BaseProps> & {
  as?: 'link'
}

type ButtonAsExternal = BaseProps & Omit<AnchorHTMLAttributes<HTMLAnchorElement>, keyof BaseProps> & {
  as?: 'external'
}
export type ButtonProps = ButtonAsAction | ButtonAsExternal | ButtonAsLink

export const Button: FC<ButtonProps> = (props) => {
  const { as, className } = props
  const classBtn = `${as === 'button' ? 'flex justify-center items-center' : ''} font-Noto text-light text-18 bg-gradient-to-t from-primary-300 to-primary-400 px-20 py-3.5 rounded-md ${className ?? ''}`

  if (as === 'link') {
    const { className: classDefault, as, ...rest } = props
    return (
      <Link className={classBtn} {...rest}/>
    )
  }

  if (as === 'external') {
    const {className: classDefault, as, ...rest } = props
    return (
      <a
        className={classBtn}
        target='_blank'
        rel='noopener noreferrer'
        {...rest}
      >
        {rest.children}
      </a>
    )
  }

  const { className: classDefault, as: kind, ...rest } = props as ButtonAsAction
  return (
    <button className={classBtn} {...rest} />
  );
}

export default Button;

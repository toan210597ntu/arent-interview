import { FC } from "react"
import { IRecordItem } from "@/types/record"

interface IRecordCardProps {
  item: IRecordItem
}

const RecordCard: FC<IRecordCardProps> = ({ item }) => {
  return (
    <div
      className="w-72 h-72 border-6 border-primary-300 relative bg-center bg-no-repeat flex items-center justify-center"
      style={{backgroundImage: `url('${item.image}')`}}
    >
      <div className="absolute inset-0 bg-black opacity-75 z-0"></div>
      <div className="relative z-1 text-center">
        <h3 className="text-primary-300 text-25">{item.name}</h3>
        <button className="bg-primary-400 w-40 text-14px-4 py-1 text-light mt-2.5">{item.CTA}</button>
      </div>
    </div>
  )
}

export default RecordCard
'use client';

import { FC, useState } from 'react';
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer
} from 'recharts'
import { IDataChart, ILabelChart } from '@/types/chart';

interface ILineChartProps {
  data: IDataChart[],
  labels?: ILabelChart[],
  showLegend?: Boolean
}

const LegendContent = (props: any) => {
  const { payload } = props
  console.log('props', props);

  const renderLegend = payload?.map((entry: any, index: number) => (
    <div
      key={`item-${index}`}
      className={`cursor-pointer px-5 pb-1 mr-4 rounded-full`}
      style={{backgroundColor: entry.inactive ? '#ffffff' : entry.color}}

    >
      <span className={`${entry.inactive ? 'ext-primary-300' : 'text-light'} text-description`}>{entry.value}</span>
    </div>
  ))
  return (
    <div className="flex items-center">
      {renderLegend}
    </div>
  )
}

const LineChartCustom: FC<ILineChartProps> = ({ data, labels, showLegend }) => {

  const renderLine = labels?.map(label => {
    return (<Line key={label.id} dataKey={label.name} stroke={label.stroke} strokeWidth={label.strokeWidth} />)
  })

  const selectLine = (e: any) => {
    console.log('selectLine', e)
  }


  return (
    <ResponsiveContainer width="100%" height="100%">
      <LineChart width={572} height={268} data={data}
        margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
        <CartesianGrid horizontal={false} />
        <XAxis tick={{ fill: '#ffffff', fontSize: 12, fontWeight: 400, color: '#777777' }} axisLine={false} tickLine={false} dataKey="name" />
        <Tooltip />
        { renderLine }
        { showLegend ?
          <Legend content={<LegendContent/>} /> : null
        }

      </LineChart>
    </ResponsiveContainer>
  )
}

export default LineChartCustom
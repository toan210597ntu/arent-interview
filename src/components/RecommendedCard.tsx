import { FC } from "react"
import { IRecommendedItem } from "@/types/recommended"

interface IRecommendedCardProps {
  item: IRecommendedItem
}

const RecommendedCard: FC<IRecommendedCardProps> = ({ item }) => {
  return (
    <div className="bg-dark py-6 px-2 text-center">
      <h3 className="text-primary-300 font-Inter text-22">{item.name}</h3>
      <div className="h-[1px] w-14 bg-light mx-auto my-2.5"></div>
      <p className="text-18 text-light font-Noto">{item.descriptions}</p>
    </div>
  )
}

export default RecommendedCard
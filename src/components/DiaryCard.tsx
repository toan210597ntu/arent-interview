import { FC } from "react"
import { IDiaryItem } from "@/types/diary"

interface IDiaryCardProps {
  item: IDiaryItem
}

const DiaryCard: FC<IDiaryCardProps> = ({ item }) => {
  return (
    <div className="border-2 border-dark-700 p-4 pb-8">
      <h3 className="text-title">
        { item.date }<br/>
        { item.time }
      </h3>
      <p className="font-Noto mt-2 text-11 text-dark-500 line-clamp-7">
        { item.title }<br/>
        { item.descriptions }
      </p>
    </div>
  )
}

export default DiaryCard
import { ICategoryItem } from "@/types/category"
import { categories } from "@/mock/categories"
import CategoryItem from "./CategoryItem"

const Categories = () => {

  const renderCategories = categories?.map((category: ICategoryItem) => {
    return (<CategoryItem key={category.id} item={category} />)
  })

  return (
    <div className="flex justify-center gap-x-21">
      { renderCategories }
    </div>
  )
}

export default Categories
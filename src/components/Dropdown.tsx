import { FC } from "react"

import { dropdowns } from "@/mock/layout"
import DropdownItem from "./DropdownItem"

interface IDropdownProps {
  className?: string;
}

const Dropdown: FC<IDropdownProps> = ({ className }) => {
  const renderDropdown = dropdowns?.map(dropdown => {
    return (<DropdownItem key={dropdown.id} item={dropdown}/>)
  })
  return (
    <div className={`bg-gray-400 ${className}`}>
      {renderDropdown}
    </div>
  )
}

export default Dropdown
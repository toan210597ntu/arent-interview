import { FC, ReactNode } from "react"
interface IBoxRecordProps {
  children: ReactNode,
  title: string;
  date: string;
  className?: string;
}

const BoxRecord: FC<IBoxRecordProps> = ({children, title, date, className}) => {
  return (
    <div className={`bg-dark-500 p-4 ${className}`}>
      <div className="flex items-center">
        <span className="text-15 font-normal text-light mr-6" dangerouslySetInnerHTML={{ __html: title}}></span>
        <span className="text-22 font-normal text-light">{ date }</span>
      </div>
      {children}
    </div>
  )
}

export default BoxRecord
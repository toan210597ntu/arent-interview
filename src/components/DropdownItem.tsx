import { FC } from "react"
import Link from "next/link"
import { IDropdownItem } from "@/types/layout"

interface IDropdownItemProps {
  item: IDropdownItem
}

const DropdownItem: FC<IDropdownItemProps> = ({ item }) => {
  return (
    <Link
      href={item.link}
      className="bg-gray-400 py-6 px-8 border-b border-b-light text-Noto text-18 block"
    >
      { item.name }
    </Link>
  )
}

export default DropdownItem
import { FC } from "react"

interface IBadgeProps {
  children: React.ReactNode;
  color?: string;
}

const Badge: FC<IBadgeProps> = ({ children, color }) => {
  return (
    <div className="relative">
      {children}
      <div
        className={`absolute rounded-full py-1 px-2 bottom-4 right-0 ${color || 'bg-primary-500'} text-badge text-light font-Inter`}
      >
        <span>1</span>
      </div>
    </div>
  )
}

export default Badge
import { FC } from "react"
import { IFoodItem } from "@/types/food"

interface IFoodCardProps {
  item: IFoodItem
}

const FoodCard: FC<IFoodCardProps> = ({ item }) => {
  return (
    <div
      className="w-58.5 h-58.5 bg-cover bg-no-repeat bg-center flex items-end cursor-pointer"
      style={{backgroundImage: `url('${item.image}')`}}
    >
      <span className="text-15 text-light font-Inter p-2 bg-primary-300">
        { item.date }.{item.category.name}
      </span>
    </div>
  )
}

export default FoodCard
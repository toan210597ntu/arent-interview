
# Arent Interview

This is document for What I do and How I structure project.


## Infomation

- [Nguyễn Văn Toàn](https://toannguyen.vercel.app/vi)
- [Source Code](https://gitlab.com/toan210597ntu/arent-interview)
- [Preview Link](https://arent-interview.vercel.app/)


## Tech Stack

**Client:** React, NextJS, Recharts, TailwindCSS.

**Deploy:** Vercel


## Documentation

#### Structure
- public: contain image, font, favicon.
- src: root folder.
    - app: Render UI foreach page.
    - components: Create util component reuseable.
    - mock: fake data.
    - modules: Create modules foreach page (contain component).
    - pages: just for create api (SSR).
    - styles: add some special style and tailwindcss.
    - types: define type foreach enity.



## Run Locally

Clone the project

```bash
  git clone
```

Go to the project directory

```bash
  cd my-project
```

Install dependencies

```bash
  npm install
```

Start the server

```bash
  npm run dev
```


## 🚀 About Me
I'm Nguyễn Văn Toàn - a full stack developer...


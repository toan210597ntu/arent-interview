const defaultTheme = require('tailwindcss/defaultTheme');
/** @type {import('tailwindcss').Config} */

module.exports = {
  content: [
    "./src/app/**/*.{js,ts,jsx,tsx}", // Note the addition of the `app` directory.
    "./src/pages/**/*.{js,ts,jsx,tsx}",
    "./src/components/**/*.{js,ts,jsx,tsx}",
    "./src/modules/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        sans: ['Inter', 'serif', ...defaultTheme.fontFamily.sans],
        Noto: ['Noto Sans JP', 'sans-serif', ...defaultTheme.fontFamily.sans],
      },
      container: {
        center: true,
        screens: {
          'xl': '960px',
        }
      },
      colors: {
        'light': '#FFFFFF',
        'primary-300': '#FFCC21',
        'primary-400': '#FF963C',
        'primary-500': '#EA6C00',
        'secondary-300': '#8FE9D0',
        'dark': '#2E2E2E',
        'dark-500': '#414141',
        'gray-400': '#777777',
        'dark-700': '#707070'
      },
      spacing: {
        '1.1': '11px',
        '5.5': '22px',
        '6.5': '25px',
        '7.5': '26px',
        '8.5': '30px',
        '15': '60px',
        '21': '84px',
        '25': '102px',
        '29': '116px',
        '33.5': '134px',
        '36': '144px',
        '40': '160px',
        '48': '192px',
        '46.5': '185px',
        '58.5': '234px',
        '70': '280px',
        '78': '312px'
      },
      fontSize: {
        'menu': ['16px', {
          lineHeight: '23px',
          fontWeight: 300
        }],
        'badge': ['10px', {
          lineHeight: '12px',
          fontWeight: 400
        }],
        'title': ['18px', {
          lineHeight: '22px',
          fontWeight: 400,
          letterSpacing: '0.09px'
        }],
        'description': ['15px', {
          lineHeight: '22px',
          fontWeight: 300,
          letterSpacing: '0.075px'
        }],
        'cta': ['12px', {
          lineHeight: '22px',
          fontWeight: 300,
          letterSpacing: '0.06px'
        }],
        '11': ['11px', {
          lineHeight: '16px',
          fontWeight: 300,
          letterSpacing: '0.033px'
        }],
        '12': ['12px', {
          lineHeight: '17px',
          fontWeight: 300,
          letterSpacing: '0.006px'
        }],
        '14': ['14px', {
          lineHeight: '20px',
          fontWeight: 300,
        }],
        '15': ['15px', {
          lineHeight: '18px',
          fontWeight: 300,
          letterSpacing: '0.15px'
        }],
        '18': ['18px', {
          lineHeight: '26px',
          fontWeight: 300,
        }],
        '20': ['20px', {
          lineHeight: '24px',
          fontWeight: 300
        }],
        '22': ['22px', {
          lineHeight: '27px',
          fontWeight: 400,
          letterSpacing: '0.11px'
        }],
        '25': ['25px', {
          lineHeight: '30px',
          letterSpacing: '0.125px',
          fontWeight: 400,
        }],
      },
      borderWidth: {
        '6': '24px'
      }
    },
  },
  plugins: [],
}
